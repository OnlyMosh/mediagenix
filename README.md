Technical assessment for Mediagenix

## Getting Started

USING NODE 18.15.0

```bash
nvm install v18.15.0
nvm use
```

Install dependencies:

```bash
npm install
```

First, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
