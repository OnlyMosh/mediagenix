import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { QueryClient, QueryClientProvider } from "react-query";

import CreateEventForm from "./index";

const SCHEMA_MOCK = [
    {
        name: "title",
        label: "Title",
        component: "text",
        required: true,
    },
    {
        name: "type",
        component: "select",
        label: "Type",
        options: [
            {
                label: "Generic",
                value: "generic",
            },
            {
                label: "Holiday",
                value: "holiday",
            },
        ],
    },
    {
        name: ["startDate", "endDate"],
        component: "range_picker",
        label: "Date",
    },
    {
        name: "description",
        label: "Description",
        component: "textarea",
    },
];

const queryClient = new QueryClient();
const Wrapper = ({ children }: any) => <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>;

describe("CreateEventForm", () => {
    beforeAll(() => {
        Object.defineProperty(window, "matchMedia", {
            writable: true,
            value: jest.fn().mockImplementation((query) => ({
                matches: false,
                media: query,
                onchange: null,
                addListener: jest.fn(), // Deprecated
                removeListener: jest.fn(), // Deprecated
                addEventListener: jest.fn(),
                removeEventListener: jest.fn(),
                dispatchEvent: jest.fn(),
            })),
        });
    });

    it("renders without crashing", () => {
        render(
            <Wrapper>
                <CreateEventForm schema={SCHEMA_MOCK} closeModal={() => {}} />
            </Wrapper>,
        );

        const titleInput = screen.getByLabelText("Title");

        fireEvent.change(titleInput, { target: { value: "Test" } });

        expect(screen.getByLabelText("Title")).toBeInTheDocument();
        expect(screen.getByLabelText("Title")).toHaveValue("Test");

        expect(screen.getByLabelText("Type")).toBeInTheDocument();
        expect(screen.getByLabelText("Date")).toBeInTheDocument();
        expect(screen.getByLabelText("Description")).toBeInTheDocument();

        expect(screen.getByText("Submit")).toBeInTheDocument();
    });

    it("adds validation when required field is empty", async () => {
        render(
            <Wrapper>
                <CreateEventForm schema={SCHEMA_MOCK} closeModal={() => {}} />
            </Wrapper>,
        );

        const submitButton = screen.getByRole("button");

        fireEvent.click(submitButton);

        await waitFor(() => expect(screen.getByText("Required")).toBeInTheDocument());
    });

    it("shows an alert when form is not filled in", async () => {
        render(
            <Wrapper>
                <CreateEventForm schema={SCHEMA_MOCK} closeModal={() => {}} />
            </Wrapper>,
        );

        const submitButton = screen.getByRole("button");

        fireEvent.click(submitButton);

        await waitFor(() =>
            expect(screen.getByText("There are errors in the form. Please correct before saving")).toBeInTheDocument(),
        );
    });
});
