import { Alert, Button, DatePicker, Form, Input, Select } from "antd";
import { Dayjs } from "dayjs";
import { useState } from "react";
import { useMutation, useQueryClient } from "react-query";

import { EventSchemaItem, EventValues } from "@/mocks/types";

const { RangePicker } = DatePicker;
const { TextArea } = Input;

interface CreateEventFormProps {
    schema: EventSchemaItem[];
    closeModal: () => void;
}

interface FormValues {
    title: string;
    description: string;
    type: "generic" | "holiday";
    dates: [Dayjs, Dayjs];
}

const getFormComponent = (field: EventSchemaItem) => {
    const { name, label, component, required, ...rest } = field;

    const commonProps = {
        name: name,
        label: label,
        rules: [{ required, message: "Required" }],
        ...rest,
    };

    switch (component) {
        case "select":
            return (
                <Form.Item {...commonProps}>
                    <Select {...rest} />
                </Form.Item>
            );
        case "range_picker":
            return (
                <Form.Item {...commonProps} name="dates">
                    <RangePicker {...rest} format={"YYYY-MM-DD"} />
                </Form.Item>
            );
        case "textarea":
            return (
                <Form.Item {...commonProps}>
                    <TextArea />
                </Form.Item>
            );
        default:
            return (
                <Form.Item {...commonProps}>
                    <Input {...rest} />
                </Form.Item>
            );
    }
};

const CreateEventForm = ({ schema, closeModal }: CreateEventFormProps) => {
    const [form] = Form.useForm();
    const queryClient = useQueryClient();
    const [didFail, setDidFail] = useState<boolean>(false);

    const postEvent = (eventValues: EventValues) =>
        fetch("/events/create", { method: "POST", body: JSON.stringify(eventValues) });

    const { isLoading, mutate } = useMutation({
        mutationFn: (values: EventValues) => postEvent(values),
        onSuccess: () => {
            setDidFail(false);
            queryClient.invalidateQueries("events").then(() => {
                closeModal();
                form.resetFields();
            });
        },
    });

    const onFinish = (values: FormValues) => {
        const transformedValues = {
            title: values.title,
            description: values.description,
            type: values.type,
            startDate: values.dates ? values.dates[0].format("YYYY-MM-DD") : undefined,
            endDate: values.dates ? values.dates[1].format("YYYY-MM-DD") : undefined,
        };

        mutate(transformedValues);
    };

    const onFinishFailed = () => setDidFail(true);

    return (
        <Form
            form={form}
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
        >
            {schema.map((field) => {
                return <div key={field.name.toString()}>{getFormComponent(field)}</div>;
            })}
            {didFail && <Alert message="There are errors in the form. Please correct before saving" type="error" />}
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button type="primary" htmlType="submit" disabled={isLoading}>
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
};

export default CreateEventForm;
