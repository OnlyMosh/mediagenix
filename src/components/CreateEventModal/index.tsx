import { Modal } from "antd";

import CreateEventForm from "@/components/CreateEventForm";
import { EventSchemaItem } from "@/mocks/types";

interface CreateEventModalProps {
    schema: EventSchemaItem[];
    isActive: boolean;
    handleCancel: () => void;
}

const CreateEventModal = ({ schema, isActive, handleCancel }: CreateEventModalProps) => {
    return (
        <Modal title="Create Event" open={isActive} onCancel={handleCancel} footer={null}>
            <CreateEventForm schema={schema} closeModal={handleCancel} />
        </Modal>
    );
};

export default CreateEventModal;
