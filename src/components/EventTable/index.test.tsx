import { render, screen } from "@testing-library/react";
import { QueryClient, QueryClientProvider } from "react-query";

import { Event } from "@/mocks/types";

import EventTable from "./index";

const SCHEMA_MOCK = [
    {
        name: "title",
        label: "Title",
        component: "text",
        required: true,
    },
    {
        name: "type",
        component: "select",
        label: "Type",
        options: [
            {
                label: "Generic",
                value: "generic",
            },
            {
                label: "Holiday",
                value: "holiday",
            },
        ],
    },
    {
        name: ["startDate", "endDate"],
        component: "range_picker",
        label: "Date",
    },
    {
        name: "description",
        label: "Description",
        component: "textarea",
    },
];
const DATA_MOCK: Event[] = [
    {
        id: "1",
        title: "A Start of the year",
        type: "generic",
        startDate: "2022-01-01",
        endDate: "2022-12-01",
        description: "This is an event about the start of this year",
    },
    {
        id: "2",
        title: "Z Mediagenix holiday",
        type: "holiday",
        startDate: "2022-04-04", //format in YYYY-MM-DD
        endDate: "2022-04-05",
        description: "Celebrating Mediagenix",
    },
    {
        id: "3",
        title: "B Some other",
        type: "holiday",
        startDate: "2022-04-04",
        endDate: "2022-04-05",
        description: "some description",
    },
];

const queryClient = new QueryClient();
const Wrapper = ({ children }: any) => <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>;

describe("EventTable", () => {
    beforeAll(() => {
        Object.defineProperty(window, "matchMedia", {
            writable: true,
            value: jest.fn().mockImplementation((query) => ({
                matches: false,
                media: query,
                onchange: null,
                addListener: jest.fn(), // Deprecated
                removeListener: jest.fn(), // Deprecated
                addEventListener: jest.fn(),
                removeEventListener: jest.fn(),
                dispatchEvent: jest.fn(),
            })),
        });
    });

    it("renders without crashing", () => {
        render(
            <Wrapper>
                <EventTable schema={SCHEMA_MOCK} dataSource={DATA_MOCK} />
            </Wrapper>,
        );

        expect(screen.getByText("A Start of the year")).toBeInTheDocument();
        expect(screen.getByText("Z Mediagenix holiday")).toBeInTheDocument();
        expect(screen.getByText("B Some other")).toBeInTheDocument();
    });
});
