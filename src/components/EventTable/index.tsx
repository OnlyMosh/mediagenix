import { Table } from "antd";

import { Event, EventSchemaItem } from "@/mocks/types";

interface EventTableProps {
    schema: EventSchemaItem[];
    dataSource: Event[] | undefined;
}

const getColumns = (schema: EventSchemaItem[]) => {
    return schema
        .map((item) => {
            if (typeof item.name === "object") {
                return item.name.map((name) => {
                    return {
                        title: name,
                        dataIndex: name,
                        key: name,
                    };
                });
            }

            return {
                title: item.label,
                dataIndex: item.name,
                key: item.name,
            };
        })
        .flat();
};

const EventTable = ({ dataSource, schema }: EventTableProps) => {
    return <Table columns={getColumns(schema)} dataSource={dataSource} pagination={false} />;
};

export default EventTable;
