"use client";
import { Input } from "antd";

const { Search } = Input;

interface SearchButtonProps {
    onChange: (value: string) => void;
    value: string;
}

const SearchButton = ({ onChange, value }: SearchButtonProps) => (
    <Search
        style={{ width: "350px" }}
        placeholder="Search events"
        onChange={(e) => onChange(e.target.value)}
        value={value}
        enterButton
    />
);

export default SearchButton;
