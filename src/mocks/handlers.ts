import { rest } from "msw";

import { Event, EventSchemaItem, EventValues } from "./types";

const eventSchema: EventSchemaItem[] = [
    {
        name: "title",
        label: "Title",
        component: "text",
        required: true,
    },
    {
        name: "type",
        component: "select",
        label: "Type",
        options: [
            {
                label: "Generic",
                value: "generic",
            },
            {
                label: "Holiday",
                value: "holiday",
            },
        ],
    },
    {
        name: ["startDate", "endDate"],
        component: "range_picker",
        label: "Date",
    },
    {
        name: "description",
        label: "Description",
        component: "textarea",
    },
];

const events: Event[] | null = [
    {
        id: "1",
        title: "A Start of the year",
        type: "generic",
        startDate: "2022-01-01",
        endDate: "2022-12-01",
        description: "This is an event about the start of this year",
    },
    {
        id: "2",
        title: "Z Mediagenix holiday",
        type: "holiday",
        startDate: "2022-04-04", //format in YYYY-MM-DD
        endDate: "2022-04-05",
        description: "Celebrating Mediagenix",
    },
    {
        id: "3",
        title: "B Some other",
        type: "holiday",
        startDate: "2022-04-04",
        endDate: "2022-04-05",
        description: "some description",
    },
];

export const handlers = [
    rest.get("/schema/event", async (req, res, ctx) => {
        return res(ctx.status(200), ctx.json<EventSchemaItem[]>(eventSchema));
    }),

    rest.post("/events", async (req, res, ctx) => {
        const { searchQuery }: { searchQuery: string } = await req.json();

        return res(
            ctx.status(200),
            ctx.json<Event[]>(
                events.filter(
                    (event) =>
                        event.title.toLowerCase().includes(searchQuery.toLowerCase()) ||
                        event.description.toLowerCase().includes(searchQuery.toLowerCase()),
                ),
            ),
        );
    }),

    rest.post("/events/create", async (req, res, ctx) => {
        const { title, type, startDate, endDate, description }: EventValues = await req.json();

        const event: Event = {
            id: (events.length + 1).toString(),
            title,
            type,
            startDate,
            endDate,
            description,
        };

        events.push(event);

        return res(ctx.status(200), ctx.json<Event>(event));
    }),
];
