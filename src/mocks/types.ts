//type First<T extends any[]> = T extends [infer U, ...any[]] ? U : never;

export type EventSchemaItem = {
    name: string | string[];
    label: string;
    component: string;
    required?: boolean;
    options?: { label: string; value: string }[];
};

export type Event = {
    id: string;
    title: string;
    type: "generic" | "holiday";
    startDate?: string;
    endDate?: string;
    description: string;
};

export type EventValues = Omit<Event, "id">;
