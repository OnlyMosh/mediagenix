import { AppProps } from "next/app";
import Head from "next/head";
import { QueryClient, QueryClientProvider } from "react-query";

if (process.env.NEXT_PUBLIC_API_MOCKING === "enabled") {
    require("../mocks");
}

const queryClient = new QueryClient();

export default function App({ Component, pageProps }: AppProps) {
    return (
        <QueryClientProvider client={queryClient}>
            <Head>
                <title>Mediagenix | CRUD</title>
                <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
                <link key="favicon" rel="icon" href="/favico.ico" sizes="any" type="image/svg+xml" />
            </Head>
            <Component {...pageProps} />
        </QueryClientProvider>
    );
}
