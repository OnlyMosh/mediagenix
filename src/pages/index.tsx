"use client";

import { useDebounce } from "@uidotdev/usehooks";
import { Button, Spin } from "antd";
import { useEffect, useState } from "react";
import { useQuery } from "react-query";

import { CreateEventModal, EventTable, SearchInput } from "@/components";
import { Event, EventSchemaItem } from "@/mocks/types";

if (process.env.NEXT_PUBLIC_API_MOCKING === "enabled") {
    require("@/mocks");
}

export default function Home() {
    const [searchQuery, setSearchQuery] = useState<string>("");
    const [isModalActive, setModalActive] = useState<boolean>(false);
    const debouncedSearchTerm = useDebounce<string>(searchQuery, 300);

    const fetchEventSchema = () => fetch("/schema/event").then((res) => res.json());
    const fetchEvents = () =>
        fetch("/events", { method: "POST", body: JSON.stringify({ searchQuery }) }).then((res) => res.json());

    const { data, refetch } = useQuery<Event[]>("events", fetchEvents);
    const { data: schema } = useQuery<EventSchemaItem[]>("schema", fetchEventSchema);

    useEffect(() => {
        refetch();
    }, [refetch, debouncedSearchTerm]);

    const handleSearchChange = (value: string) => setSearchQuery(value);

    return (
        <main>
            {schema ? (
                <>
                    <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "25px" }}>
                        <SearchInput onChange={handleSearchChange} value={searchQuery} />
                        <Button onClick={() => setModalActive(true)}>Create event</Button>
                    </div>
                    <EventTable schema={schema} dataSource={data} />
                    <CreateEventModal
                        isActive={isModalActive}
                        handleCancel={() => setModalActive(false)}
                        schema={schema}
                    />
                </>
            ) : (
                <Spin />
            )}
        </main>
    );
}
